﻿namespace Grammar.MathSample;

/// <summary>
///     參考來源
///     Allen Kuo - C#入門 2021
/// </summary>
public static class MathOverFlow
{
    /// <summary>
    ///     因為發生溢位，所以回傳的數值變成1。
    /// </summary>
    /// <param name="number"></param>
    public static void MathOverflow(int number = int.MaxValue)
    {
        int result = number * number;
        Console.WriteLine($"發生溢位情況，因此數值為 {result}。");
    }
    
    /// <summary>
    ///     使用Checked，在溢位時會拋出Exception。
    ///     ref https://docs.microsoft.com/zh-tw/dotnet/csharp/language-reference/keywords/checked
    /// </summary>
    /// <param name="number"></param>
    public static void MathCheck(int number = int.MaxValue)
    {
        checked
        {
            
            int result2 = number * number;
            Console.WriteLine(result2);   
        }
    }
}